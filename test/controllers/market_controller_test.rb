require_relative '../test_helper'

class MarketControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "index" do
    should "require sign in" do
      get :index
      assert_response :redirect
    end
    
    should "render" do
      sign_in FactoryGirl.create(:user)
      get :index
      assert_response :success
    end
  end
  
  context "purchase" do
    should "execute action_method" do
      market_item = FactoryGirl.create(:market_item, :give_edit, gabloons: 3, frammes: 3)
      user = FactoryGirl.create(:user, gabloons: 50, frammes: 50)
      assert_equal 0, user.edits
      sign_in user
      get :purchase, id: market_item.id
      user = user.reload
      assert_equal 1, user.edits
      assert_equal 47, user.gabloons
      assert_equal 47, user.frammes
    end
  end
end
