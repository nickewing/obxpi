require_relative '../test_helper'

class PostsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "edit" do
    should "render for user with edits" do
      sign_in FactoryGirl.create(:user, edits: 1)
      post = FactoryGirl.create(:post)
      get :edit, forum_id: post.topic.forum.id, topic_id: post.topic.id, id: post.id
      assert_response :success
    end
    
    should "not render for user with no edits" do
      sign_in FactoryGirl.create(:user, edits: 0)
      post = FactoryGirl.create(:post)
      get :edit, forum_id: post.topic.forum.id, topic_id: post.topic.id, id: post.id
      assert_response :redirect
    end
  end
  
  context "create" do
    should "require sign in" do
      topic = FactoryGirl.create(:topic)
      post :create, forum_id: topic.forum.id, topic_id: topic.id
      assert_response :redirect
    end
    
    should "add post to topic" do
      topic = FactoryGirl.create(:topic)
      sign_in topic.user
      assert_difference "Post.count", 1 do
        post :create, forum_id: topic.forum.id, topic_id: topic.id, post: { body: "hi!" }
      end
    end
    
    should "add mentions in post body" do
      topic = FactoryGirl.create(:topic)
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)
      sign_in topic.user
      body_with_mentions = "@#{user1.name} @#{user2.name}"
      assert_difference "Mention.count", 2 do
        post :create, forum_id: topic.forum.id, topic_id: topic.id, post: { body: body_with_mentions }
      end
    end
    
    should "get 6 gabloon for a post" do
      user = FactoryGirl.create(:user, gabloons: 0)
      topic = FactoryGirl.create(:topic, user: user)
      sign_in user
      post :create, forum_id: topic.forum.id, topic_id: topic.id, post: { body: "hi!" }
      assert_equal 6, user.reload.gabloons
    end
  end
  
  context "update" do
    should "change record if user has edits" do
      post = FactoryGirl.create(:post, body: "no")
      user = FactoryGirl.create(:user, edits: 1)
      sign_in user
      put :update, forum_id: post.topic.forum.id, topic_id: post.topic.id, id: post.id, post: { body: "oh hellow" }
      post.reload
      assert_equal "oh hellow", post.body
      user.reload
      assert_equal 0, user.edits
    end
    
    should "not change record if user does not have edits" do
      post = FactoryGirl.create(:post, body: "no")
      user = FactoryGirl.create(:user, edits: 0)
      sign_in user
      put :update, forum_id: post.topic.forum.id, topic_id: post.topic.id, id: post.id, post: { body: "sciences" }
      post.reload
      assert_equal "no", post.body
      user.reload
      assert_equal 0, user.edits
    end
  end
end
