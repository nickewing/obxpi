require_relative '../test_helper'

class PagesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "create" do
    should "redirect if user_id is not current_user" do
      user = FactoryGirl.create(:user)
      sign_in user
      post :create, user_id: (user.id + 1)
      assert_response :redirect
    end
    
    should "make a page" do
      user = FactoryGirl.create(:user)
      sign_in user
      refute user.page
      assert_difference 'Page.count', 1 do
        post :create, user_id: user.id
      end
      assert user.reload.page
    end
  end
  
  context "show" do
    should "not require login" do
      user = FactoryGirl.create(:user)
      get :show, user_id: user.id
      assert_response :success
      assert_template :show
    end
    
    should "find user by name" do
      user = FactoryGirl.create(:user)
      get :show, user_name: user.name
      assert_response :success
      assert_template :show
    end
  end
  
  context "destroy" do
    should "redirect if user_id is not current_user" do
      user = FactoryGirl.create(:user)
      sign_in user
      delete :destroy, user_id: (user.id + 1)
      assert_response :redirect
    end
    
    should "remove the page record" do
      page = FactoryGirl.create(:page)
      page_id = page.id
      sign_in page.user
      delete :destroy, user_id: page.user.id 
      assert_equal 0, Page.where(id: page_id).count
    end
  end
  
  context "update" do
    should "redirect if user_id is not current_user" do
      user = FactoryGirl.create(:user)
      sign_in user
      put :update, user_id: (user.id + 1), page: { markdown: "stuff", style: "#id{}" }
      assert_response :redirect
    end
    
    should "change page markdown" do
      page = FactoryGirl.create(:page)
      sign_in page.user
      put :update, user_id: page.user.id, page: { markdown: "stuff!!!", style: "#id{}" }
      page.reload
      assert_equal "stuff!!!", page.markdown
      assert_equal "#id{}", page.style
    end
  end
end
