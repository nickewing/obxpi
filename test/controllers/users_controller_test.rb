require_relative '../test_helper'

class UsersControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @user = FactoryGirl.create(:user, name: "steve")
    sign_in @user
  end
  
  context "show" do
    should "flag mentions as seen if current_user is user_id" do
      mention1 = FactoryGirl.create(:mention, user: @user)
      mention2 = FactoryGirl.create(:mention, user: @user)
      assert_equal 2, @user.mentions.unseen.count
      get :show, id: @user.id
      assert_equal 0, @user.reload.mentions.unseen.count
    end
  end
  
  context "update" do
    should "update logged in user" do
      post :update, id: @user.id, user: { name: "newName" }
      assert_response :redirect
      assert_equal "newName", @user.reload.name
    end
    
    should "not update other user" do
      user = FactoryGirl.create(:user, name: "dave")
      post :update, id: user.id, user: { name: "nopeName" }
      assert_equal "dave", user.reload.name
      assert_response :redirect
    end
  end
end