require_relative '../test_helper'

class ShoutsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "index" do
    should "give json of shouts" do
    end
  end
  
  context "edit" do
    should "render for user with edits" do
      sign_in FactoryGirl.create(:user, edits: 1)
      shout = FactoryGirl.create(:shout)
      get :edit, id: shout.id
      assert_response :success
    end
    
    should "not render for user with no edits" do
      sign_in FactoryGirl.create(:user, edits: 0)
      shout = FactoryGirl.create(:shout)
      get :edit, id: shout.id
      assert_response :redirect
    end
  end
  
  context "update" do
    should "change record if user has edits" do
      shout = FactoryGirl.create(:shout, body: "green")
      user = FactoryGirl.create(:user, edits: 1)
      sign_in user
      put :update, id: shout.id, shout: { body: "a new body" }
      shout.reload
      assert_equal "a new body", shout.body
      user.reload
      assert_equal 0, user.edits
    end
    
    should "not change record if user does not have edits" do
      shout = FactoryGirl.create(:shout, body: "green")
      user = FactoryGirl.create(:user, edits: 0)
      sign_in user
      put :update, id: shout.id, shout: { body: "a new body" }
      shout.reload
      assert_equal "green", shout.body
      user.reload
      assert_equal 0, user.edits
    end
  end
end
