require_relative '../test_helper'

class MoneyTransactionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "POST create" do
    context "with valid params" do
      should "give money to user" do
        sign_in FactoryGirl.create(:user, gabloons: 10, frammes: 10)
        to_user = FactoryGirl.create(:user, gabloons: 5, frammes: 5)
        post :create, money_transaction: { gabloons: 10, frammes: 7, to_user: to_user.name }
        to_user.reload
        assert_equal 15, to_user.gabloons, "should change gabloons"
        assert_equal 12, to_user.frammes, "should change frammes"
      end
    end
    
    context "with insufficient funds" do
      should "not give money to user" do
        sign_in FactoryGirl.create(:user, gabloons: 0, frammes: 0)
        to_user = FactoryGirl.create(:user, gabloons: 5, frammes: 5)
        post :create, money_transaction: { gabloons: 1, frammes: 1, to_user: to_user.name }
        to_user.reload
        assert_equal 5, to_user.gabloons, "should change gabloons"
        assert_equal 5, to_user.frammes, "should change frammes"
      end
    end
  end
  
end
