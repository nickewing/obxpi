require_relative '../test_helper'

class ArticlesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "index" do
    should "render for admin" do
      sign_in FactoryGirl.create(:user, :admin)
      get :index
      assert_response :success
    end
    
    should "redirect for non admin" do
      sign_in FactoryGirl.create(:user)
      get :index
      assert_response :redirect
    end
  end
  
  context "new" do
    should "render for admin" do
      sign_in FactoryGirl.create(:user, :admin)
      get :new
      assert_response :success
    end
    
    should "redirect for non admin" do
      sign_in FactoryGirl.create(:user)
      get :new
      assert_response :redirect
    end
  end
  
  context "edit" do
    should "render for admin" do
      sign_in FactoryGirl.create(:user, :admin)
      article = FactoryGirl.create(:article)
      get :edit, id: article.id
      assert_response :success
    end
    
    should "redirect for non admin" do
      sign_in FactoryGirl.create(:user)
      article = FactoryGirl.create(:article)
      get :edit, id: article.id
      assert_response :redirect
    end
    
    should "render for non admin with edits" do
      sign_in FactoryGirl.create(:user, edits: 2)
      article = FactoryGirl.create(:article)
      get :edit, id: article.id
      assert_response :success
    end
  end
  
  context "update" do
    should "change record if user has edits" do
      article = FactoryGirl.create(:article, blurp: "change this")
      user = FactoryGirl.create(:user, edits: 1)
      sign_in user
      put :update, id: article.id, article: { blurp: "sciences" }
      article.reload
      assert_equal "sciences", article.blurp
      user.reload
      assert_equal 0, user.edits
    end
    
    should "change record if user is admin and has no edits" do
      article = FactoryGirl.create(:article, blurp: "change this")
      user = FactoryGirl.create(:user, :admin, edits: 0)
      sign_in user
      put :update, id: article.id, article: { blurp: "a great day for great" }
      article.reload
      assert_equal "a great day for great", article.blurp
      user.reload
      assert_equal 0, user.edits
    end
  end
  
end
