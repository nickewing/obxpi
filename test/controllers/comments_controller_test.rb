require_relative '../test_helper'

class CommentsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "index" do
    should "render" do
      article = FactoryGirl.create(:article)
      get :index, article_id: article.id
    end
  end
  
  context "edit" do
    should "render for user with edits" do
      sign_in FactoryGirl.create(:user, edits: 1)
      comment = FactoryGirl.create(:comment)
      get :edit, article_id: comment.article.id, id: comment.id
      assert_response :success
    end
    
    should "not render for user with no edits" do
      sign_in FactoryGirl.create(:user, edits: 0)
      comment = FactoryGirl.create(:comment)
      get :edit, article_id: comment.article.id, id: comment.id
      assert_response :redirect
    end
  end
  
  context "create" do 
    setup do
      sign_in FactoryGirl.create(:user)
    end
    
    should "add comment to article" do
      article = FactoryGirl.create(:article)
      assert_difference 'Comment.count', 1 do
        post :create, article_id: article.id, comment: { body: "this is comment" }
      end
    end
    
    should "generate mentions in comment" do
      article = FactoryGirl.create(:article)
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)
      user3 = FactoryGirl.create(:user)
      body_with_mentions = "@#{user1.name} @#{user2.name} @#{user3.name}"
      assert_difference 'Mention.count', 3 do
        post :create, article_id: article.id, comment: { body: body_with_mentions }
      end
    end
      
  end
  
  context "update" do
    should "change record if user has edits" do
      user = FactoryGirl.create(:user, edits: 2)
      comment = FactoryGirl.create(:comment, body: "my comment", user: user)
      sign_in user
      put :update, article_id: comment.article.id, id: comment.id, comment: { body: "hi!" }
      comment.reload
      assert_equal "hi!", comment.body
      user.reload
      assert_equal 1, user.edits
    end
    
    should "not change record if user does not have edits" do
      user = FactoryGirl.create(:user, :admin, edits: 0)
      comment = FactoryGirl.create(:comment, body: "my comment", user: user)
      sign_in user
      put :update, article_id: comment.article.id, id: comment.id, comment: { body: "hi!" }
      comment.reload
      assert_equal "my comment", comment.body
      user.reload
      assert_equal 0, user.edits
    end
  end
  
end
