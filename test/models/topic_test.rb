require_relative '../test_helper'

class TopicTest < ActiveSupport::TestCase
  should validate_presence_of(:body)
  should validate_presence_of(:name)
  should belong_to(:forum)
  should belong_to(:user)
  should have_many(:posts)
  should have_many(:mentions)
end
