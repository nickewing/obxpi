require_relative '../test_helper'

class CommentTest < ActiveSupport::TestCase
  should validate_presence_of(:body)
  should belong_to(:article)
  should belong_to(:user)
  should have_many(:mentions)
end
