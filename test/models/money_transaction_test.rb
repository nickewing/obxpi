require_relative '../test_helper'

class MoneyTransactionTest < ActiveSupport::TestCase
  should belong_to(:from_user)
  should belong_to(:to_user)
end
