require_relative '../test_helper'

class ArticleTest < ActiveSupport::TestCase
  should validate_presence_of(:title)
  should validate_presence_of(:blurp)
  should belong_to(:user)
  should have_many(:comments)
end
