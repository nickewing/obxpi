require_relative '../test_helper'

class UserTest < ActiveSupport::TestCase
  should have_many(:articles)
  should have_many(:shouts)
  should have_many(:topics)
  should have_many(:posts)
  should have_many(:comments)
  should have_many(:mentions)
  should have_one(:page)
  should validate_presence_of(:name)
  should validate_uniqueness_of(:name)
  should ensure_length_of(:name).is_at_most(20)
  
  context "name" do
    should "not allow spaces" do
      user = FactoryGirl.build(:user)
      user.name = "test name"
      refute user.valid?
    end
  end
end
