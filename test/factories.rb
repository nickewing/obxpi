FactoryGirl.define do
  
  factory :user do
    sequence(:name) { |n| "John#{n}" }
    sequence(:email) { |n| "test#{n}@test#{n}.com" }
    password 'testpassword'
    password_confirmation 'testpassword'
    admin false
    
    trait :admin do
      admin true
    end
  end
  
  factory :shout do
    body "look at this shout! hurray!"
    user
  end
  
  factory :article do
    sequence(:title) { |n| "Article ##{n}" }
    blurp "article that is neat"
    user
  end
  
  factory :mention do
    association :mentionable, factory: :shout
    user
  end
  
  factory :forum do
    sequence(:name) { |n| "Forum #{n}" }
  end
  
  factory :topic do
    forum
    user
    sequence(:name) { |n| "Topic #{n}" }
    body "my topic is about stuff and this is the body"
  end
  
  factory :post do
    topic
    user
    body "post body"
  end
  
  factory :page do
    markdown "# stuffyay"
    user
  end
  
  factory :market_item do
    sequence(:name) { |n| "Market Item ##{n}" }
    description "buy this item!"
    gabloons 100
    frammes 3
    
    trait :give_edit do
      name "Buy Edits"
      action_method "give_edit"
    end
  end
  
  factory :comment do
    article
    body "a comment"
    user
  end
  
end