require 'simplecov'
SimpleCov.start

ENV["RAILS_ENV"] ||= "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require 'shoulda/context'
require 'factory_girl'

FactoryGirl.find_definitions

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!
end
