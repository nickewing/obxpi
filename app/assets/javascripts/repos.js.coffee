class @Repo
  constructor: (content_types, post_url) ->    
    $('#new_repo').submit (e) ->
      e.preventDefault()
    
    progress_template = "
      <script id=\"template-upload\" type=\"text/x-tmpl\">
      <div id=\"file-{%=o.unique_id%}\" class=\"upload\">
        {%=o.name%}
        <div class=\"progress\"><div class=\"bar\" style=\"width: 0%\"></div></div>
      </div>
      </script>"
    
    $("#upload-template").html progress_template
    
    $("#s3-uploader").S3Uploader
      allow_multiple_files: false
      before_add: (file) ->
        title = $('#repo_title').val()
        tiny_url = $('#repo_tiny_url').val()
        if content_types.indexOf(file.type) > -1
          if title
            $.post('/repos', {repo: { title: title, tiny_url: tiny_url, unique_id: file.unique_id }});
            return true
          else
            $('#upload-error-message').html('Need a title!')
            return false
        else
          $('#upload-error-message').html('File type not allowed!')
          return false
  
    $('#s3-uploader').bind "s3_upload_complete", (e, content) ->
      location.reload()