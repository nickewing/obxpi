class @Users
  get_user_info: =>
    $.getJSON "/user_info.json", (data) =>
      if data.mentions_count > 0
        document.title = "ObX (" + data.mentions_count + ")"
      $('.online-users-count').html(data.online_users)
      $('.mentions-count').html(data.mentions)
      setTimeout (=> @get_user_info()), 6000
    
  start: =>    
    setTimeout (=> @get_user_info()), 6000

class @UserPageEdit
  constructor: (@ace) ->
    editor = @ace.edit "user-markdown"
    editor.setTheme "ace/theme/twilight"
    editor.getSession().setMode "ace/mode/markdown"
    editor.getSession().setTabSize 2
    editor.getSession().setUseSoftTabs true

    textarea = $('textarea[name="page[markdown]"]')
    textarea.hide()
    editor.getSession().setValue textarea.val()
    
    editor2 = @ace.edit "user-style"
    editor2.setTheme "ace/theme/twilight"
    editor2.getSession().setMode "ace/mode/scss"
    editor2.getSession().setTabSize 2
    editor2.getSession().setUseSoftTabs true

    textarea2 = $('textarea[name="page[style]"]')
    textarea2.hide()
    editor2.getSession().setValue textarea2.val()
    
    $('form.edit_page').submit =>
      textarea.val editor.getSession().getValue() 
      textarea2.val editor2.getSession().getValue()