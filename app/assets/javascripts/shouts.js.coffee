class @Shouts
  generate_shout: (shout) =>
    " <div class=\"comment a-#{shout.id}\">
        <div class=\"avatar_wrap\">
          <div class=\"avatar\">
            <img src=\"#{shout.avatar_url}\" />
          </div>
        </div>
        #{shout.user_link}
        - 
        #{shout.shout_body}
        #{shout.edit_link}
      </div>"

  get_shouts: =>
    $.getJSON "/shouts.json", (data) =>
      shout_html = ""
      for shout in data
        shout_html = shout_html + @generate_shout(shout)
    
      $('.shouts-wrapper').html(shout_html)
      $('.comment:odd').removeClass('selected')
      $('.comment:even').addClass('selected')
      setTimeout (=> @get_shouts()), 5000
  
  start: =>    
    setTimeout (=> @get_shouts()), 5000
    $('#shout_body').focus()