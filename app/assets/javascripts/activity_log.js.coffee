class @ActivityLog
  generate_log: (activity_log) =>
      "<div class=\"stream_activity_wrap\">
        <div class=\"avatar_wrap\">
          <div class=\"avatar\">
            <img src=\"#{activity_log.avatar_url}\" />
          </div>
        </div>
        <p>
          #{activity_log.body}
        </p>
      </div>
    
      <br />"

  get_activity_log: =>
    $.getJSON "/activity_log.json", (data) =>
      activity_log_html = ""
      for activity_log in data
        activity_log_html = activity_log_html + @generate_log(activity_log)
    
      $("#recent_activity").html(activity_log_html)
      setTimeout (=> @get_activity_log()), 5000
  
  start: =>
    $("#sticker").sticky({ topSpacing: 0 })
    @get_activity_log()