require 'twitter-text'

class TwitterFormatting
  include Twitter::Autolink
end

module ApplicationHelper
  def active_class_if(ele, eq)
    if ele == eq
      'active'
    else
      ''
    end
  end
  
  def money_string(obj)
    "#{obj.gabloons}.g #{obj.frammes}.f"
  end
  
  def break_formatter(text)
    raw(h(text).gsub(/\n/, '<br />'))
  end
  
  def extract_and_format_urls(text)
    TwitterFormatting.new.auto_link_urls(text)
  end
  
  def format_and_link_user(user)
    opts = {}
    opts = {class: 'red_it'} if user.red_it?
    if user_signed_in? && (current_user.id == user.id)
      link = link_to(user.name, edit_user_path(user), opts)
    else
      link = link_to(user.name, user_path(user), opts)
    end
    if user.pet.blank?
      pet = ""
    else
      pet = " (#{user.pet})"
      if user_signed_in? && (current_user.pet_poison > 0)
        pet_link = link_to "#", poison_pet_path(user)
        pet = raw(pet + " " + pet_link)
      end
    end
    raw(link + pet)
  end
  
  def format_model_body(model, options = {})
    text = model.body

    if model.respond_to?(:mentions)
      text = link_mentions(text, model.mentions)
    end

    if options[:kramdown_format]
      text = kramdown_format(text)
    end

    emoji_format(text)
  end

  def repo_url_link(repo)
    if repo.tiny_url.present?
      url = tiny_url_repo_url(repo.tiny_url)
    else
      url = download_repo_url(repo)
    end
    link_to url, url
  end
  
  #
  # thanks to: https://www.ruby-forum.com/topic/126876
  #
  def readable_file_size(size)
    if size
      units = %w{B KB MB GB TB}
      e = (Math.log(size)/Math.log(1024)).floor
      s = "%.1f" % (size.to_f / 1024**e)
      s.sub(/\.?0*$/, units[e])
    end
  end
  
  def mentionable_summary(mention)
    mentionable = mention.mentionable
    user = mentionable.user
    summary = ""
    case mentionable
    when Shout
      link1 = link_to "shoutbox", shouts_path
      link2 = link_to user.name, user_path(user)
      summary = "in the #{link1} by #{link2}"
    when Topic
      link1 = link_to mentionable.name, forum_topic_path(mentionable.forum, mentionable)
      link2 = link_to mentionable.user.name, user_path(mentionable.user)
      summary = "in #{link1} by #{link2}"
    when Post
      link1 = link_to user.name, user_path(mentionable.user)
      link2 = link_to mentionable.topic.name, forum_topic_path(mentionable.topic.forum, mentionable.topic)
      summary = "by #{link1} in response to #{link2}"
    when Comment
      link1 = link_to user.name, user_path(mentionable.user)
      link2 = link_to mentionable.article.title, article_comments_path(mentionable.article)
      summary = "by #{link1} as a comment on #{link2}"
    end
    raw(summary)
  end
  
  def format_activity_log(activity_log)
    return unless activity_log
    format_text = ""
    user = activity_log.user
    link1 = format_and_link_user(user)
    case activity_log.record_type
    when 'shout'
      link2 = link_to "shoutbox", shouts_path
      format_text = "#{link1} said something in the #{link2}"
    when 'topic'
      topic = Topic.find(activity_log.record_id)
      link2 = link_to topic.name, forum_topic_path(topic.forum, topic)
      format_text = "#{link1} created a topic #{link2}"
    when 'post'
      post = Post.find(activity_log.record_id)
      link2 = link_to post.topic.name, forum_topic_path(post.topic.forum, post.topic)
      format_text = "#{link1} replied to #{link2}"
    when 'link'
      if link = Link.where(id: activity_log.record_id).first
        link2 = link_to link.title, link_path(link)
        format_text = "#{link1} submitted a new link #{link2}"
      end
    when 'page'
      if page = Page.where(id: activity_log.record_id).first
        link2 = link_to page.url, page.url
        format_text = "#{link1} created a new page #{link2}"
      end
    when 'comment'
      if comment = Comment.where(id: activity_log.record_id).first
        link2 = link_to comment.article.title, article_comments_path(comment.article)
        format_text = "#{link1} commented on #{link2}"
      end
    when 'repo'
      if repo = Repo.where(id: activity_log.record_id).first
        repo_size = readable_file_size(repo.document_file_size)
        link2 = repo_url_link(repo)
        format_text = "#{link1} uploaded a #{repo_size} #{repo.document_content_type} file #{link2}"
      end
    end
    raw(format_text)
  end

  private

  def kramdown_format(text)
    raw(Kramdown::Document.new(text).to_html)
  end

  def link_mentions(text, mentions)
    return_text = h(text)
    mentions.each do |mention|
      mention_text = "@#{mention.user.name}"
      user_link = link_to(mention_text, user_path(mention.user))
      return_text = return_text.gsub(mention_text, user_link)
    end
    raw(extract_and_format_urls(return_text))
  end

  def emoji_format(content)
    h(content).to_str.gsub(/:([a-z0-9\+\-_]+):/) do |match|
      if Emoji.names.include?($1)
        %|<img alt="#{$1}" height="20" src="/images/emoji/#{$1}.png" style="vertical-align:middle" width="20" />|
      else
        match
      end
    end.html_safe if content.present?
  end
end
