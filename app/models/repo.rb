class Repo < ActiveRecord::Base
  belongs_to :user
  
  has_attached_file :document,
   storage: :s3,
   bucket: "obx.io",
   s3_permissions: :private,
   s3_credentials: { access_key_id: ENV['AWS_ACCESS_KEY_ID'], secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'] },
   path: "d/#{Rails.env}/repo/:id"
  
  VALID_DOCUMENT_TYPES = %w{image/jpeg image/png image/gif audio/mp4 audio/mp3
                            audio/ogg audio/mpeg video/mp4 video/mpeg audio/wav
                            video/ogg video/quicktime video/x-flv application/ogg
                            application/gzip application/zip application/postscript application/pdf
                            application/json text/css text/html audio/vnd.wave}
  
  validates_attachment_size :document, less_than: 100.megabyte
  validates_attachment_content_type :document, content_type: VALID_DOCUMENT_TYPES
  
  validates_presence_of :title
  
  validates_uniqueness_of :tiny_url, allow_blank: true, allow_nil: true
  validates_format_of :tiny_url, with: /\A[a-zA-Z\d\s]*\z/, allow_blank: true, allow_nil: true
  
  validates_uniqueness_of :unique_id, allow_nil: true
  
  scope :all_with_documents, -> { where('document_file_size is not null').order(created_at: :desc) }
  
  def user_can_delete?(u)
    u && (u.id == self.user_id)
  end
  
  def move_tmp_s3_file(tmp_path)
    escaped_tmp_path = URI.unescape(tmp_path)
    new_file_path = "d/#{Rails.env}/repo/#{self.id}"
    tmp_path_split = escaped_tmp_path.split("/obx.io/").last

    s3 = AWS::S3.new
    if s3.buckets["obx.io"].objects[new_file_path].copy_from(tmp_path_split)
      s3.buckets["obx.io"].objects[tmp_path_split].delete
    end
  end
end
