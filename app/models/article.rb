class Article < ActiveRecord::Base
  validates_presence_of :title, :blurp
  
  belongs_to :user
  
  has_many :comments
end
