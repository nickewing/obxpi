class Link < ActiveRecord::Base
  belongs_to :user
  
  validates_presence_of :title
  
  validates_presence_of :url
  validates_uniqueness_of :url
  
  validate format: { with: /https?:\/\/[\S]+/ }
  
  scope :top, -> { order('votes DESC') }
  scope :newest, -> { order('created_at DESC') }
  scope :today, -> { where('created_at > ?', 1.day.ago) }
  scope :week, -> { where('created_at > ?', 1.week.ago) }
  scope :month, -> { where('created_at > ?', 1.month.ago) }
  
  def self.for_time_and_order(time = "today", order = "top")
    case order
    when "top"
      links = Link.top
    when "new"
      links = Link.newest
    else
      links = Link.top
    end
    
    case time
    when "today"
      links = links.today
    when "week"
      links = links.week
    when "month"
      links = links.month
    when "all"
      links = links.all
    else
      links = links.today
    end
    
    links
  end
  
  def user_can_vote?(user)
    Vote.where(user_id: user.id, link_id: self.id).count <= 0
  end
  
  def vote_with_user(user)
    if user_can_vote?(user)
      if user.gabloons > 10
        if user.take_gabloons(10)
          give_vote(1)
          Vote.create(user: user, link: self)
          self.user.give_gabloons(10)
        end
      end
    end
  end
  
  def give_vote(amount)
    self.update_attribute(:votes, self.votes + amount)
  end
end
