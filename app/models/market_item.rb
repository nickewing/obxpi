class MarketItem < ActiveRecord::Base
  
  ACTION_METHODS = [ :give_edit, :give_pet, :give_pet_poison, :red_it ]
  
  validates_presence_of :name
  validates_presence_of :description
  
  validates :frammes, numericality: { only_integer: true }
  validates :gabloons, numericality: { only_integer: true }
  validates :action_method, inclusion: { in: ACTION_METHODS.map(&:to_s) }
  
  def purchase!(user)
    if (user.gabloons >= self.gabloons) && (user.frammes >= self.frammes)
      user.gabloons -= self.gabloons
      user.frammes -= self.frammes
      if user.save
        self.send(self.action_method, user.id)
      end
    end
  end
  
  def give_edit(user_id)
    User.find(user_id).give_edits(1)
  end
  
  def give_pet(user_id)
    User.find(user_id).generate_pet
  end
  
  def give_pet_poison(user_id)
    User.find(user_id).give_pet_poison(1)
  end
  
  def red_it(user_id)
    User.find(user_id).red_it!
  end
end
