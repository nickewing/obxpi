class MoneyTransaction < ActiveRecord::Base
  belongs_to :from_user, class_name: User
  belongs_to :to_user, class_name: User
  
  validates :frammes, numericality: { only_integer: true }
  validates :gabloons, numericality: { only_integer: true }
  
  validate :sufficient_funds
  
  def transact!
    from_user = self.from_user
    to_user = self.to_user
    
    if (from_user.gabloons >= self.gabloons) && (from_user.frammes >= self.frammes)    
      to_user.gabloons += self.gabloons
      to_user.frammes += self.frammes
    
      from_user.gabloons -= self.gabloons
      from_user.frammes -= self.frammes
      
      if to_user.save && from_user.save
        touch(:completed_at)
      else
        touch(:failed_at)
      end
    else
      touch(:failed_at)
    end
  end
  
  def sufficient_funds
    from_user = User.find(self.from_user_id)
    if from_user.gabloons < self.gabloons
      errors.add(:gabloons, "insufficient funds")
    end
    if from_user.frammes < self.frammes
      errors.add(:frammes, "insufficient funds")
    end
  end
end
