class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_many :articles
  has_many :shouts
  has_many :topics
  has_many :posts
  has_many :comments
  has_many :mentions
  has_many :links
  has_many :votes
  has_many :activity_logs
  
  has_one :page
  
  has_attached_file :avatar, styles: { thumb: "100x100>" }, default_url: "/images/:style/default.png"
  
  NAME_REGEX = /\A[a-zA-Z0-9_]+\Z/
  ILLEGAL_NAMES = %w{admin www}
  PETS = %w{-.- o.O ^.^ O.O -_- ;-; o.o @_@ -,- ~_~ #_#}
  
  validates_presence_of :name
  validates_format_of :name, with: NAME_REGEX
  validates_length_of :name, maximum: 20
  validates_uniqueness_of :name
  validates :name, exclusion: { in: ILLEGAL_NAMES }
  
  validates_attachment_size :avatar, less_than: 1.megabyte
  validates_attachment_content_type :avatar, content_type: ['image/jpeg', 'image/png', 'image/gif']
  
  validates :frammes, numericality: { only_integer: true }
  validates :gabloons, numericality: { only_integer: true }
  
  scope :online, -> { where("last_seen > ?", 5.minutes.ago) }
  
  def flag_mentions_as_viewed!
    mentions.unseen.each do |mention|
      mention.touch(:viewed_at)
    end
  end
  
  def give_gabloons(amount)
    update_attribute(:gabloons, self.gabloons + amount)
  end
  
  def give_frammes(amount)
    update_attribute(:frammes, self.frammes + amount)
  end
  
  def give_edits(amount)
    update_attribute(:edits, self.edits + amount)
  end
  
  def give_pet_poison(amount)
    update_attribute(:pet_poison, self.pet_poison + amount)
  end
  
  def take_gabloons(amount)
    update_attribute(:gabloons, self.gabloons - amount)
  end
  
  def take_edit!
    update_attribute(:edits, self.edits - 1)
  end
  
  def take_pet_poison!
    update_attribute(:pet_poison, self.pet_poison - 1)
  end
  
  def poison_pet!
    update_attribute(:pet, "")
  end
  
  def generate_pet
    update_attribute(:pet, PETS[rand(PETS.length) - 1])
  end
  
  def red_it!
    update_attribute(:red_it, true)
  end
  
  def score
    last_seen_score = ((Time.now.to_i - self.last_seen.to_i) * 0.0000002).to_i
    framme_score    = (frammes > 1) ? frammes : (frammes + 1)
    gabloon_score   = (gabloons / 10).to_i
    activity_score  = comments.count + articles.count + shouts.count + topics.count + posts.count
    
    ((activity_score * framme_score) + gabloon_score) - last_seen_score
  end
  
  def last_activity
    activity_logs.order(created_at: :desc).first
  end
end
