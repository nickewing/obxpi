class Topic < ActiveRecord::Base
  validates_presence_of :name, :body
  
  belongs_to :forum
  belongs_to :user
  
  has_many :posts
  has_many :mentions, as: :mentionable
  
  def last_post
    posts.order(updated_at: :desc).first
  end
end
