class Post < ActiveRecord::Base
  validates_presence_of :body
  
  belongs_to :topic
  belongs_to :user
  
  has_many :mentions, as: :mentionable
end
