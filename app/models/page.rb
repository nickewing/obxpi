class Page < ActiveRecord::Base
  belongs_to :user
  
  def formatted_markdown
    Kramdown::Document.new(self.markdown).to_html if self.markdown
  end
  
  def formatted_style
    Sass::Engine.new(self.style, syntax: :scss).render if self.style
  end
  
  def url
    "http://" + user.name.downcase + ".obx.io"
  end
end
