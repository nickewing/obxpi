class ActivityLog < ActiveRecord::Base
  belongs_to :user
  
  VALID_TYPES = %w{topic shout post page link comment repo}
  
  validates_presence_of :record_type
  validates :record_type, inclusion: { in: VALID_TYPES }
  
  validates_presence_of :record_id
  
  scope :stream, -> { order(created_at: :desc).limit(12) }
  
  def self.log(type, id, user)
    self.create(record_type: type, record_id: id, user: user)
  end
  
  def self.remove_log(type, id)
    if log = self.where(record_type: type, record_id: id).first
      log.destory
    end
  end
end
