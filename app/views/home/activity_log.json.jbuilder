json.array! @activity_log do |activity_log|
  json.avatar_url activity_log.user.avatar.url(:thumb)
  json.body format_activity_log(activity_log)
end