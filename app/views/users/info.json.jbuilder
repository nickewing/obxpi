json.users_online (pluralize(@users_online, "User") + " Online")
json.mentions pluralize(@mentions, "Mention")
json.mentions_count @mentions