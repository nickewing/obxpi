json.array! @shouts do |shout|
  json.shout_body format_model_body(shout)
  json.user_link  format_and_link_user(shout.user)
  json.avatar_url shout.user.avatar.url(:thumb)
  json.id         shout.id
  json.edit_link ( user_has_edits? ? link_to("Edit", edit_shout_path(shout)) : "" )
end
