class ReposController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @repos = Repo.all_with_documents
  end
  
  def create
    repo = Repo.new(repo_params)
    repo.user = current_user
    repo.save
    redirect_to repos_path
  end
  
  def download
    repo = find_repo
    redirect_to repo.document.expiring_url(30)
  end
  
  def destroy
    repo = find_repo
    if repo.destroy
      ActivityLog.remove_log('repo', repo.id)
      flash[:notice] = "Repo gone for good."
    else
      flash[:notice] = "We still got your repo...something broke and could not remove it."
    end
    redirect_to repos_path
  end
  
  def show
    @repo = find_repo
  end
  
  def edit
    @repo = find_repo
  end
  
  def update
    repo = find_repo
    repo.update_attributes(updatable_repo_params)
    redirect_to repo_path(repo)
  end
  
  def s3_callback
    if repo = Repo.where(unique_id: unique_id).first
      repo.document_file_name = file_name
      repo.document_content_type = filetype
      repo.document_file_size = file_size
      repo.document_updated_at = Time.now
      if repo.save
        repo.move_tmp_s3_file(tmp_path)
        ActivityLog.log('repo', repo.id, current_user)
        flash[:notice] = "Upload successful! That was neat."
      else
        flash[:notice] = "Something happened when trying to save that repo."
      end
    end
    render json: 'ok'
  end
  
  private
  
  def find_repo
    repo = Repo.where(tiny_url: tiny_url).first if tiny_url
    repo ||= Repo.find(repo_id)
    repo
  end
  
  def file_name
    params[:filename]
  end
  
  def file_size
    params[:filesize]
  end
  
  def filetype
    params[:filetype]
  end
  
  def tiny_url
    params[:tiny_url]
  end
  
  def tmp_path
    params[:filepath]
  end
  
  def unique_id
    params[:unique_id]
  end
  
  def repo_id
    params[:id]
  end
  
  def repo_params
    params.require(:repo).permit(:title, :tiny_url, :unique_id)
  end
  
  def updatable_repo_params
    params.require(:repo).permit(:title, :tiny_url)
  end
end
