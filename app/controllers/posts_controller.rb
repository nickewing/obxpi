class PostsController < ApplicationController
  before_filter :authenticate_user!, only: [:create]
  before_filter :ensure_user_has_edits, only: [:edit, :update]

  def create
    forum = Forum.find(forum_id)
    topic = forum.topics.find(topic_id)
    post = topic.posts.new(post_params)
    post.user = current_user
    if post.save
      Mention.generate(post)
      current_user.give_gabloons(6)
      ActivityLog.log('post', post.id, current_user)
      flash[:notice] = "Your post has been saved and hopefully retreivable."
    else
      flash[:notice] = "Things broke and that post was not saved."
    end
    redirect_to forum_topic_path(forum, topic)
  end
  
  def edit
    @post = Post.find(post_id)
  end
  
  def update
    post = Post.find(post_id)
    if post.update_attributes(post_params)
      Mention.generate(post)
      current_user.take_edit!
    end
    redirect_to forum_topic_path(post.topic.forum, post.topic)
  end
  
  private
  
  def post_id
    params[:id]
  end
  
  def post_params
    params.require(:post).permit(:body)
  end
  
  def forum_id
    params[:forum_id]
  end
  
  def topic_id
    params[:topic_id]
  end
end
