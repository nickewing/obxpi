class LinksController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  
  def index
    @order = order || 'top'
    @time = time || 'all'
    @links = Link.for_time_and_order(@time, @order)
  end
  
  def create
    link = current_user.links.new(link_params)
    if link.save
      current_user.give_gabloons(5)
      ActivityLog.log('link', link.id, current_user)
      flash[:notice] = "Link saved...yup."
    else
      flash[:notice] = "...something went wrong, brah."
    end
    redirect_to links_path
  end
  
  def vote
    link = Link.find(link_id)
    link.vote_with_user(current_user)
    redirect_to session[:return_to]
  end
  
  def show
    @link = Link.find(link_id)
  end
  
  private
  
  def link_params
    params.require(:link).permit(:title, :url)
  end
  
  def link_id
    params[:id]
  end
  
  def order
    params[:order]
  end
  
  def time
    params[:time]
  end
end
