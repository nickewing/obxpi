class CommentsController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  before_filter :ensure_user_has_edits, only: [:edit, :update]
  
  def index
    @article = Article.find(article_id)
    @comments = @article.comments.order(created_at: :desc)
  end
  
  def create
    article = Article.find(article_id)
    comment = article.comments.new(comment_params)
    comment.user = current_user
    if comment.save
      Mention.generate(comment)
      ActivityLog.log('comment', comment.id, current_user)
      current_user.give_gabloons(2)
      flash[:notice] = "Your reply is appreciated, probably."
    else
      flash[:notice] = "Your reply was not saved. Ha."
    end
    redirect_to article_comments_path(article)
  end
  
  def edit
    @comment = Comment.find(comment_id)
  end
  
  def update
    comment = Comment.find(comment_id)
    if comment.update_attributes(comment_params)
      Mention.generate(comment)
      current_user.take_edit!
    end
    redirect_to article_comments_path(comment.article)
  end
  
  private
  
  def comment_id
    params[:id]
  end
  
  def article_id
    params[:article_id]
  end
  
  def comment_params
    params.require(:comment).permit(:body)
  end
end
