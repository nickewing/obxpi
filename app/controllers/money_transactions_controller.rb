class MoneyTransactionsController < ApplicationController
  before_filter :authenticate_user!
  
  def create
    user_name = money_transaction_params[:to_user]
    gabloons = money_transaction_params[:gabloons]
    frammes = money_transaction_params[:frammes]
    if user = User.where(name: user_name).first
      money_transaction = MoneyTransaction.new(gabloons: gabloons, frammes: frammes, to_user_id: user.id, from_user_id: current_user.id)
      if money_transaction.save
        money_transaction.transact!
      end
    end
    redirect_to edit_user_path(current_user)
  end
  
  private
  
  def money_transaction_params
    params.require(:money_transaction).permit(:gabloons, :frammes, :to_user)
  end
end
