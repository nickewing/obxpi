class ForumsController < ApplicationController
  before_filter :authenticate_user!, only: [:create]
  before_filter :admin_user?, only: [:create]
  
  def index
    @forums = Forum.all
  end
  
  def show
    @forum = Forum.find(forum_id)
    @topics = @forum.topics.order(created_at: :desc)
  end
  
  def create
    forum = Forum.new(forum_params)
    forum.save
    redirect_to admin_path
  end
  
  private
  
  def forum_params
    params.require(:forum).permit(:name)
  end
  
  def forum_id
    params[:id]
  end
end
