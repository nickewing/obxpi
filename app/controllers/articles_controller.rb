class ArticlesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_user?, except: [:edit, :update]
  before_filter :ensure_admin_or_editable, only: [:edit, :update]
  
  def index
    @articles = Article.order(created_at: :desc)
  end
  
  def new
    @article = Article.new
  end
  
  def create
    article = current_user.articles.new(article_params)
    if article.save
      redirect_to articles_path
    else
      render :new
    end
  end
  
  def edit
    @article = Article.find(article_id)
    render :new
  end
  
  def update
    article = Article.find(article_id)
    article.update_attributes(article_params)
    if current_user.admin?
      redirect_to articles_path
    else
      current_user.take_edit!
      redirect_to root_path
    end
  end
  
  def destroy
    article = current_user.articles.find(article_id)
    article.comments.each do |comment|
      comment.mentions.each do |mention|
        mention.destroy
      end
      comment.destroy
    end
    article.destroy
    redirect_to articles_path
  end
  
  private
  
  def ensure_admin_or_editable
    ensure_user_has_edits unless current_user.admin?
  end
  
  def article_params
    params.require(:article).permit(:title, :blurp)
  end
  
  def article_id
    params[:id]
  end
end
