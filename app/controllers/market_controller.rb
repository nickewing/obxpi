class MarketController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @market_items = MarketItem.all
  end
  
  def purchase
    market_item = MarketItem.find(market_id)
    if market_item.purchase!(current_user)
      flash[:notice] = "Thank you, come again!"
    end
    redirect_to market_path
  end
  
  private
  
  def market_id
    params[:id]
  end
end
