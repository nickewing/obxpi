class HomeController < ApplicationController
  before_filter :check_subdomain_for_user, only: [:index]
  
  def index
    @articles = Article.order(created_at: :desc).limit(7)
    @activity_log = ActivityLog.stream
  end
  
  def online_users
    @users = User.online
  end
  
  def activity_log
    @activity_log = ActivityLog.stream
    
    respond_to do |format|
      format.json
    end
  end
  
  def stream
    @activity_log = ActivityLog.stream
    render layout: 'stream'
  end
  
  private
  
  def check_subdomain_for_user
    subdomain = request.subdomains.first
    if subdomain =~ User::NAME_REGEX && (!User::ILLEGAL_NAMES.include?(subdomain))
      user = User.where('lower(name) = ?', subdomain).first
      if user && user.page
        @page = user.page.formatted_markdown
        @user = user
        @style = user.page.formatted_style
        render '/pages/show', layout: "user_page"
      end
    end
  end
  
end
