class UsersController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  
  def index
    @users = User.all
  end
  
  def show
    @user = User.find(user_id)
    if current_user == @user
      current_user.flag_mentions_as_viewed!
    end
    @mentions = @user.mentions.order(created_at: :desc)
  end
  
  def edit
    @user = current_user
  end
  
  def update
    user = current_user
    if user.id.to_s == user_id.to_s
      if user.update_attributes(user_params)
        sign_in user, bypass: true
        flash[:notice] = "Your account was updated!"
        redirect_to edit_user_path(user)
      else
        flash[:notice] = "Bad stuff happened and nothing saved."
      end
    else
      redirect_to root_path
    end
  end
  
  def info
    @users_online = User.online.count
    @mentions = current_user.mentions.unseen.count
    respond_to do |format|
      format.json
    end
  end
  
  def gabloons_to_frammes
    if current_user.gabloons >= 1000
      current_user.give_frammes(1)
      current_user.take_gabloons(1000)
    end
    redirect_to edit_user_path(current_user)
  end
  
  def poison_pet
    if current_user.pet_poison > 0
      user = User.find(user_id)
      if user.poison_pet!
        current_user.take_pet_poison!
      end
    end
    redirect_to session[:return_to]
  end
  
  private
  
  def user_id
    params[:id]
  end
  
  def user_name
    params[:user_name]
  end
  
  def user_params
    params.require(:user).permit(:name, :avatar, :password, :password_confirmation)
  end
end
