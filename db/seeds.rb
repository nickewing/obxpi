# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(email: 'robertpeterson@gmail.com', password: 'asdasdasd', password_confirmation: 'asdasdasd', admin: true, name: "robert")
user2 = User.create(email: 'jon@tanner.com', password: 'testing123', password_confirmation: 'testing123', admin: true, name: "tanman")

Article.create(title: 'New Article', blurp: 'sup', user: user)
Article.create(title: 'New stuff', blurp: 'Hey everybody, how are you doing?', user: user)
Article.create(title: 'People are no on fire', blurp: 'Please get the water plz everybody is dying', user: user)
Article.create(title: 'Lets talk about new stuff', blurp: 'I think you know where this is going ;)', user: user)
Article.create(title: 'Crapped my pants', blurp: 'I am crying so hard right now.  Does anybody have some dried corn?', user: user)
Article.create(title: 'RobertOS 50.1.54 released', blurp: 'What a cool OS.  You cannot do anything except for stuff that is already done', user: user)
Article.create(title: 'Wanna buy my stuff?', blurp: 'Just go to www.greatstuff.com and order your stuff today.', user: user)
Article.create(title: 'This is the end', blurp: 'Goodbye, I love you.', user: user)

forum = Forum.create(name: "General")
forum.topics.create(name: "Topic!", body: "sup boy", user: user)
forum.topics.create(name: "Some pretty general shit", body: "Holler at a son", user: user)
forum.topics.create(name: "My name is Jorbo, can I meet you today?", body: "sup boy", user: user)
forum.topics.create(name: "DAE like to go to Reddit?  LOL", body: "Wut a great website XD", user: user)

forum = Forum.create(name: "Politics and Bullshit")
forum.topics.create(name: "NOBAMA NOBAMA NOBAMA", body: "He wasn't even born on earth, idiots.", user: user)
forum.topics.create(name: "I love politics, lets talk about how to jerk each other off", body: "Yummy J/O advice!", user: user)
forum.topics.create(name: "DAE RON PAUL?", body: "Raaaaaawn Paaaaaaaul!", user: user)
forum.topics.create(name: "My Clinton shakes bring all the Bills to the yard", body: "And they be all like haha wut.", user: user)

MarketItem.create(gabloons: 0, frammes: 1, action_method: "give_edit", name: "Edit", 
  description: "Buy an Edit! This gives you 1 Edit. An Edit will give you the permission to edit a news article, comment, shout, topic, or post. Buy as many as you want!")

MarketItem.create(gabloons: 250, frammes: 0, action_method: "give_pet", name: "Pet",
  description: "Get a pet to follow you around. This will give you a random pet that lives next to you user name. What do these pets look like, you ask? But one and find out!")

MarketItem.create(gabloons: 350, frammes: 0, action_method: "give_pet_poison", name: "Pet Poison",
  description: "The best way to kill a pet is with pet poison. Need I say more?")

MarketItem.create(gabloons: 500, frammes: 10, action_method: "red_it", name: "Red It",
  description: "Your name will be colored red. Everywhere.")