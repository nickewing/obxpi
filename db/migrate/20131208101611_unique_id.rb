class UniqueId < ActiveRecord::Migration
  def change
    change_table :repos do |t|
      t.string :unique_id
    end
  end
end
