class AddEditsToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.integer :edits, default: 0, null: false
    end
  end
end
