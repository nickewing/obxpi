class AddPetToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :pet
    end
  end
end
