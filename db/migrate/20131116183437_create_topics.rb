class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :name
      t.text :body
      t.references :forum
      t.references :user
      t.timestamps
    end
  end
end
