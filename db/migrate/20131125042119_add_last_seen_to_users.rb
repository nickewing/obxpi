class AddLastSeenToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.timestamp :last_seen
    end
  end
end
