class CreateRepos < ActiveRecord::Migration
  def change
    create_table :repos do |t|
      t.string :title
      t.string :tiny_url
      t.references :user
      t.attachment :document
      t.timestamps
    end
  end
end
