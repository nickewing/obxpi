class ChangeLongColumns < ActiveRecord::Migration
  def up
    change_column :shouts, :body, :text
    change_column :links, :url, :text
  end
  
  def down
    change_column :shouts, :body, :string
    change_column :links, :url, :string
  end
end
