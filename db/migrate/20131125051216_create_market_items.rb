class CreateMarketItems < ActiveRecord::Migration
  def change
    create_table :market_items do |t|
      t.integer :gabloons, default: 0, null: false
      t.integer :frammes, default: 0, null: false
      t.string :name
      t.string :description
      t.string :action_method
      t.timestamps
    end
  end
end
