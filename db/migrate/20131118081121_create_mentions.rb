class CreateMentions < ActiveRecord::Migration
  def change
    create_table :mentions do |t|
      t.references :user
      t.timestamp :viewed_at
      t.references :mentionable, polymorphic: true
      t.timestamps
    end
  end
end
