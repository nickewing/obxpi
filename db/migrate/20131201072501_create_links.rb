class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.references :user
      t.integer :votes, default: 0, null: false
      t.string :title
      t.string :url
      t.timestamps
    end
  end
end
