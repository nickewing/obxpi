class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.references :user
      t.text :markdown
      t.timestamps
    end
  end
end
