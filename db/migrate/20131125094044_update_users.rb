class UpdateUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.integer :pet_poison, default: 0, null: false
      t.boolean :red_it, default: false, null: false
    end
  end
end
