class AddStyleToPage < ActiveRecord::Migration
  def change
    change_table :pages do |t|
      t.text :style
    end
  end
end
