class CreateMoneyTransactions < ActiveRecord::Migration
  def change
    create_table :money_transactions do |t|
      t.integer :to_user_id
      t.integer :from_user_id
      t.integer :gabloons, default: 0
      t.integer :frammes, default: 0
      t.timestamp :completed_at
      t.timestamp :failed_at
      t.timestamps
    end
  end
end
