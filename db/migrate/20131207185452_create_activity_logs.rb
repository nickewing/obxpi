class CreateActivityLogs < ActiveRecord::Migration
  def change
    create_table :activity_logs do |t|
      t.references :user
      t.string :record_type
      t.integer :record_id
      t.timestamps
    end
  end
end
