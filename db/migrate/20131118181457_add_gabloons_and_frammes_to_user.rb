class AddGabloonsAndFrammesToUser < ActiveRecord::Migration
  def change
    add_column :users, :gabloons, :integer, null: false, default: 0
    add_column :users, :frammes, :integer, null: false, default: 0
  end
end
