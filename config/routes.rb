Obxpi::Application.routes.draw do
  
  devise_for :users, controllers: { registrations: 'users/registrations' }
  
  resources :articles do
    resources :comments
  end
  
  get "user_info", to: "users#info"
  get "user_gabloons_to_frammes", to: "users#gabloons_to_frammes"
  get "poison_pet/:id", to: "users#poison_pet", as: :poison_pet
  resources :users do
    resource :page
  end
  
  resources :forums do
    resources :topics do
      resources :posts
    end
  end
  
  resources :links do
    get 'vote', on: :member
  end
  
  resources :shouts
  
  resources :money_transactions
  
  get "r/:tiny_url", to: "repos#download", as: :tiny_url_repo
  post "repos/s3_callback", to: "repos#s3_callback", as: :s3_callback_repo
  resources :repos do
    get "download", on: :member
  end
  
  get "market", to: "market#index"
  get "market/purchase/:id", to: "market#purchase", as: :market_purchase
  
  get "misc", to: "misc#index"
  post "shout", to: "misc#shout", as: :misc_shout
  
  get "admin", to: "admin#index"
  get "admin/:user_id/make", to: "admin#make", as: :make_admin
  get "admin/:user_id/remove", to: "admin#remove", as: :remove_admin
  get "admin/edit_user/:user_id", to: "admin#edit_user", as: :edit_user_admin
  patch "admin/update_user/:user_id", to: "admin#update_user", as: :update_user_admin
  
  get "u/:user_name", to: "pages#show", as: :u_page
  
  get "online_users", to: "home#online_users", as: :online_users
  get "activity_log", to: "home#activity_log"
  get "stream", to: "home#stream"
  root to: "home#index"
  
end
